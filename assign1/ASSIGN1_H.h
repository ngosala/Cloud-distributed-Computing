/* 
 * File:   ASSIGN1_H.h
 * Author: nikhil
 *
 * Created on September 15, 2014, 1:27 PM
 */

#ifndef ASSIGN1_H_H
#define	ASSIGN1_H_H

#include <stdio.h>
#include "ERROR_CODES.h"
#include <sys/time.h>
#include <stdlib.h>

struct timeval start,end;

RC testcpu(int num_of_threads);
RC testWriteDiskSequential(char *fileName);
RC testReadDiskSequential(char *fileName);
void Run_MemBenchmark();
long random_gen(long a,long b);
RC remove_file(char *fileName);

#endif	/* ASSIGN1_H_H */

