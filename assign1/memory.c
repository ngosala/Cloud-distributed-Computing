#include <string.h>
#include <time.h>
#include <pthread.h>
#include "ASSIGN1_H.h"

void *testMemReadSequential_one_byte(void *arg);
void *testMemReadSequential_one_kb(void *arg);
void *testMemReadSequential_one_mb(void *arg);

void *testMemWriteSequential_one_byte(void *arg);
void *testMemWriteSequential_one_kb(void *arg);
void *testMemWriteSequential_one_mb(void *arg);

void *testMemReadRandom_one_byte(void *arg);
void *testMemReadRandom_one_kb(void *arg);
void *testMemReadRandom_one_mb(void *arg);

void *testMemWriteRandom_one_byte(void *arg);
void *testMemWriteRandom_one_kb(void *arg);
void *testMemWriteRandom_one_mb(void *arg);

long random_gen (long min, long max)
{
    long l=rand() % (max - min + 1) + min;
    return l;
}

void main()
{
pthread_t pth1,pth2;
int i=0;
for(i=1;i<9;i++)
{
switch(i)
      {
printf("Enter");
      case 1:
        printf("START: Memory Benchmarking: Read Sequential varying Block sizes and 1 thread\n");
      
        //Read sequential 1 Byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadSequential_one_byte,NULL);
        pthread_join(pth1,NULL);
        gettimeofday(&end, 0x0);
	printf("Latency for Block Size 1B is %f ms \n",((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))/24*1024*1024);
	printf("Sequential Read speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
       //Read sequential 1KB
	gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadSequential_one_kb,NULL);
        pthread_join(pth1,NULL);
        gettimeofday(&end, 0x0);
        printf("Sequential Read speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
       
	//Read sequential 1MB
	gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadSequential_one_mb,NULL);
        pthread_join(pth1,NULL);
        gettimeofday(&end, 0x0);
        printf("Sequential Read speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Read Sequential varying Block sizes and 1 thread\n");
     
      break;
      
      case 2: 
        printf("START: Memory Benchmarking: Read Sequential varying Block sizes and 2 thread\n");
      
        //Read sequential 1 Byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadSequential_one_byte,NULL);
        pthread_create(&pth2,NULL,testMemReadSequential_one_byte,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
        gettimeofday(&end, 0x0);
        printf("Sequential Read speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        
       //Read sequential 1KB
	gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadSequential_one_kb,NULL);
        pthread_create(&pth2,NULL,testMemReadSequential_one_kb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
        gettimeofday(&end, 0x0);
        printf("Sequential Read speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
       
	//Read sequential 1MB
	gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadSequential_one_mb,NULL);
        pthread_create(&pth2,NULL,testMemReadSequential_one_mb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
        gettimeofday(&end, 0x0);
        printf("Sequential Read speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Read Sequential varying Block sizes and 2 thread\n");

      break;
      case 3:
        printf("START: Memory Benchmarking: Write Sequential varying Block sizes and 1 thread\n");
        
 	//Write Sequential one byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteSequential_one_byte,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
	printf("Latency for Block Size 1B is %f ms \n",((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))/24*1024*1024);
        printf("Sequential Write speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one kb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteSequential_one_kb,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
        printf("Sequential Write speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one mb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteSequential_one_mb,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
        printf("Sequential Write speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Write Sequential varying Block sizes and 1 thread\n");                
      break;
      case 4:
        printf("START: Memory Benchmarking: Write Sequential varying Block sizes and 2 thread\n");
        
 	//Write Sequential one byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteSequential_one_byte,NULL);
        pthread_create(&pth2,NULL,testMemWriteSequential_one_byte,NULL);
        pthread_join(pth1,NULL);
	pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
        printf("Sequential Write speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one kb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteSequential_one_kb,NULL);
        pthread_create(&pth2,NULL,testMemWriteSequential_one_kb,NULL);
        pthread_join(pth1,NULL);
	pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
        printf("Sequential Write speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one mb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteSequential_one_mb,NULL);
	pthread_create(&pth2,NULL,testMemWriteSequential_one_mb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
        printf("Sequential Write speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Write Sequential varying Block sizes and 2 thread\n");                
      break;	  	
case 5:
printf("START: Memory Benchmarking: Read Random varying Block sizes and 1 thread\n");
        
 	//Write Sequential one byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadRandom_one_byte,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
	printf("Latency for Block Size 1B is %f ms \n",((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))/24*1024*1024);
        printf("Random Read speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one kb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadRandom_one_kb,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
        printf("Random Read speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one mb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadRandom_one_mb,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
        printf("Random Read speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Read Random varying Block sizes and 1 thread\n");                
break;
case 6:
printf("START: Memory Benchmarking: Read Random varying Block sizes and 2 thread\n");
        
 	//Write Sequential one byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadRandom_one_byte,NULL);
        pthread_create(&pth2,NULL,testMemReadRandom_one_byte,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Read speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one kb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadRandom_one_kb,NULL);
        pthread_create(&pth2,NULL,testMemReadRandom_one_kb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Read speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one mb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemReadRandom_one_mb,NULL);
        pthread_create(&pth2,NULL,testMemReadRandom_one_mb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Read speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Read Random varying Block sizes and 1 thread\n");                
break;
case 7:
printf("START: Memory Benchmarking: Write Random varying Block sizes and 1 thread\n");
        
 	//Write Sequential one byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteRandom_one_byte,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
		printf("Latency for Block Size 1B is %f ms \n",((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))/24*1024*1024);
 printf("Random Write speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one kb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteRandom_one_kb,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Write speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one mb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteRandom_one_mb,NULL);
        pthread_join(pth1,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Write speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Write Random varying Block sizes and 1 thread\n");                
break;
case 8:
printf("START: Memory Benchmarking: Write Random varying Block sizes and 2 thread\n");
        
 	//Write Sequential one byte
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteRandom_one_byte,NULL);
        pthread_create(&pth2,NULL,testMemWriteRandom_one_byte,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Write speed for Block Size 1B is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one kb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteRandom_one_kb,NULL);
        pthread_create(&pth2,NULL,testMemWriteRandom_one_kb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Write speed for Block Size 1KB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
        
	//Write Sequential one mb
        gettimeofday(&start, 0x0);
        pthread_create(&pth1,NULL,testMemWriteRandom_one_mb,NULL);
        pthread_create(&pth2,NULL,testMemWriteRandom_one_mb,NULL);
        pthread_join(pth1,NULL);
        pthread_join(pth2,NULL);
	gettimeofday(&end, 0x0);
 printf("Random Write speed for Block Size 1MB is %f MB/s \n", (double)24*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("STOP: Memory Benchmarking: Write Random varying Block sizes and 2 thread\n");                
break;
default:
break;
}
} 
}


void *testMemReadSequential_one_byte(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    static long src[25*1024*1024];
    
        for(l=0;l<25*1024*1024;l++){
        src[l]=1;
    }
        for(l=0;l<24*1024*1024;l++){
        memcpy(dPtr+l,"1",1);
        }
        free(dPtr);
}


void *testMemReadSequential_one_kb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    static long src[25*1024*1024];
    
        for(l=0;l<25*1024*1024;l++){
        src[l]=1;
    }

      for(l=0;l<24*1024;l++){
        memcpy(dPtr+l,src+l,1024);
        }
	free(dPtr);
}

void *testMemReadSequential_one_mb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    static long src[25*1024*1024];

        for(l=0;l<25*1024*1024;l++){
        src[l]=1;
    }

        for(l=0;l<24;l++){
        memcpy(dPtr,src,1048576);
        }
	free(dPtr);
}

void *testMemWriteSequential_one_byte(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);

        for(l=0;l<24*1024*1024;l++){
        memset(dPtr+l,1,1);
        }

        free(dPtr);
}

void *testMemWriteSequential_one_kb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);

     for(l=0;l<24*1024;l++){
        memset(dPtr+l,1,1024);
        }

        free(dPtr);
}

void *testMemWriteSequential_one_mb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);

        for(l=0;l<24;l++){
        memset(dPtr+l,1,1048576);
        }
        free(dPtr);
}

void *testMemReadRandom_one_byte(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    int i,j,k;
    static long src[25*1024*1024];
    
        for(l=0;l<25*1024*1024;l++){
        src[l]=1;
        }
        for(l=0;l<24*1024*1024;l++){
            i=random_gen(1,24*1024*1024);
            memcpy(dPtr+i,"1",1);
        }
        
       free(dPtr);
}

void *testMemReadRandom_one_kb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    int i,j,k;
    static long src[25*1024*1024];
    
        for(l=0;l<25*1024*1024;l++){
        src[l]=1;
        }
        
        for(l=0;l<24*1024;l++){
            j=random_gen(1,24*1024);
            memcpy(dPtr+j,src+j,1024);
        }
	free(dPtr);
}

void *testMemReadRandom_one_mb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    int i,j,k;
    static long src[25*1024*1024];
    
        for(l=0;l<25*1024*1024;l++){
        src[l]=1;
        }
        
	for(l=0;l<24;l++){
            k=random_gen(1,24);
            memcpy(dPtr+k,src+k,1048576);
        }
        free(dPtr);
}

void *testMemWriteRandom_one_byte(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    int i,j,k;

        for(l=0;l<24*1024*1024;l++){
            i=random_gen(1,24*1024*1024);
            memset(dPtr+i,1,1);
        }
        
        free(dPtr);
}

void *testMemWriteRandom_one_kb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    int i,j,k;
	   for(l=0;l<24*1024;l++){
            j=random_gen(1,24*1024);
            memset(dPtr+j,1,1024);
        }
    
        free(dPtr);
}

void *testMemWriteRandom_one_mb(void *arg){
    long l=0;
    char *dPtr =(char *)malloc(25*1024*1024);
    int i,j,k;
        for(l=0;l<24*1024;l++){
            k=random_gen(1,24);
            memset(dPtr+k,1,1048576);
        }
        free(dPtr);
}


