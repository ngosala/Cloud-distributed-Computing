package com.proj3.gae.servlets;

import java.awt.SystemColor;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

public class search extends HttpServlet {

	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
	public static long startTime=0,endTime=0;
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, NullPointerException, IOException {
		res.setContentType("text/plain");
		String strFilename = req.getParameter("key");

		res.getWriter().println("Search for file in the Cloud Storage");
		startTime=System.currentTimeMillis();
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
		int flag = 0;
		while (resultFiles.hasNext()) {
			ListItem listFile = resultFiles.next();
			String nameFile = listFile.getName().toString();
			if (nameFile.contains(strFilename)) {
				res.getWriter().println("File " + strFilename + " Found");
				flag = 1;
				break;
			}
		}
		if (flag == 0) {
			res.getWriter().println("File " + strFilename + " not Found");
		}
endTime=System.currentTimeMillis();
res.getWriter().println("Total time taken without memcache:"+((endTime-startTime)/1000)+" sec");
		res.getWriter().println(
				"Search for file in the Cloud Storage Completed");
	}
}
