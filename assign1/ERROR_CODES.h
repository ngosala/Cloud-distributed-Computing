/* 
 * File:   ERROR_CODES.h
 * Author: nikhil
 *
 * Created on September 15, 2014, 1:35 PM
 */

#ifndef ERROR_CODES_H
#define	ERROR_CODES_H

typedef int RC;

#define RC_OK 0
#define RC_FILE_NOT_FOUND 1

#endif	/* ERROR_CODES_H */

