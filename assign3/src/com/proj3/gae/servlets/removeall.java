package com.proj3.gae.servlets;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class removeall extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
	private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		res.getWriter().println("\n");
		res.getWriter().println("Remove FILE from cloud storage ");
		res.getWriter().println("\n");
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
		while (resultFiles.hasNext()) 
		{
			ListItem listFile = resultFiles.next();
			String nameFile = listFile.getName().toString();
			GcsFilename GCfilename = new GcsFilename("gaebucket", nameFile);
		boolean result=gcsService.delete(GCfilename);
		removeCache(nameFile);
		if(!result)
		{
			res.getWriter().println("\n");
			res.getWriter().println("File is deleted");
			
		}
		}
		res.getWriter().println("\n");
		res.getWriter().println("Remove-All is Completed");
	}
	
	public void removeCache(String strFilename)
	{
		if(cache.get(strFilename)!=null){
		String strFileContent = cache.get(strFilename).toString();
		if (strFileContent != null) {
			if (cache.contains(strFilename)) {
				cache.delete(strFilename);
			}
			}
	}
	}
}
