

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class ThreadExecutor {
	public static ExecutorService executor;
	
	
	public static void init(int n) {
		System.out.println("Initiating Executor Service");
		executor = Executors.newFixedThreadPool(n);
	}

public static HashMap<Integer, Integer> ExecuteTasks(HashMap<String , Integer> tasks){
	final HashMap<Integer, Integer> results = new HashMap<Integer, Integer>();
	
		for(Entry<String, Integer> entry: tasks.entrySet()){
			String task=entry.getKey();
			String[] taskC=task.split(" "); 
			final long time=Integer.parseInt(taskC[1]);
			System.out.println("Executing task "+entry.getValue());
			final int tasknum=entry.getValue();
			Future te=executor.submit(new Runnable(){
			
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						Thread.sleep(time);
						results.put(tasknum, 0);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						results.put(tasknum, 1);
					}
					
				}
				
				
			});
			
		}
		
		return results;
		
	}

public static void StopExec() {
	System.out.println("Shutting Down Executor");
	executor.shutdown();
}
	
}
