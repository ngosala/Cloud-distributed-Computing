import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.KeyPair;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;




public class AWSCreate {
	public static AmazonEC2Client amazonEC2Client;
	public static void init(){
		
		AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (C:\\Users\\nikhil\\.aws\\credentials), and is in valid format.",
                    e);
        }
		amazonEC2Client=   new AmazonEC2Client(credentials);
		amazonEC2Client.setEndpoint("ec2.us-west-2.amazonaws.com");
	}
	
	public static void CreateSecurityGroup(){
		
		// Creating Security Group
		CreateSecurityGroupRequest createSecurityGroupRequest = new CreateSecurityGroupRequest();    	
		createSecurityGroupRequest.withGroupName("Proj4SecurityGroup").withDescription("Proj4 Security Group");
		CreateSecurityGroupResult createSecurityGroupResult = amazonEC2Client.createSecurityGroup(createSecurityGroupRequest);
		
		// Allow Inbound Traffic
		
		IpPermission ipPermission = new IpPermission();
			    	
			ipPermission.withIpRanges("111.111.111.111/32", "150.150.150.150/32").withIpProtocol("tcp").withFromPort(22).withToPort(22);
			AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest =new AuthorizeSecurityGroupIngressRequest();
				    	
				authorizeSecurityGroupIngressRequest.withGroupName("Proj4SecurityGroup").withIpPermissions(ipPermission);
				amazonEC2Client.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
	}
	public static void CreateKeyPair(){
		
		CreateKeyPairRequest createKeyPairRequest = new CreateKeyPairRequest();
		createKeyPairRequest.withKeyName("proj4");
		CreateKeyPairResult createKeyPairResult = amazonEC2Client.createKeyPair(createKeyPairRequest);
		KeyPair keyPair = new KeyPair();

		keyPair = createKeyPairResult.getKeyPair();

		String privateKey = keyPair.getKeyMaterial();

	}	
	
	public static void LaunchInstances(int number_of_instances){
		
		init();
		CreateKeyPair();
		CreateSecurityGroup();
		
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId("ami-1f98cf2f")
        .withInstanceType("t2.micro")
        .withMinCount(1)
        .withMaxCount(number_of_instances)
        .withKeyName("proj4")
        .withSecurityGroups("Proj4SecurityGroup");
		
		RunInstancesResult runInstancesResult = amazonEC2Client.runInstances(runInstancesRequest);
	}
	
	public static void deleteKP_SG(){
		
		DeleteKeyPairRequest delete=new DeleteKeyPairRequest();
		delete.setKeyName("proj4");
		
	}
	

}
