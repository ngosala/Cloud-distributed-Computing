package com.proj3.gae.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Part;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.nio.ByteBuffer;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.tools.cloudstorage.*;


@SuppressWarnings("serial")
public class upload_mc extends HttpServlet{
	
	private final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	public static long startTime=0,endTime=0;
		public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		res.setContentType("text/plain");
	   	Map<String, List<BlobInfo>> blobsData = blobstoreService.getBlobInfos(req);
	   	startTime=System.currentTimeMillis();
    			 for (String key : blobsData.keySet())
    			     {
    			         for(BlobInfo blob:blobsData.get(key))
    			         {
    			        	 if(blob.getSize()< 32000000){
    			        	 BlobstoreInputStream in = new BlobstoreInputStream(blob.getBlobKey());
    			             byte[] b = new byte[(int)blob.getSize()];
    			             in.read(b);
    			             GcsService gcsService = GcsServiceFactory.createGcsService();
    			             
    			             GcsFilename filename = new GcsFilename("gaebucket",blob.getFilename());
    			             GcsFileOptions options = new GcsFileOptions.Builder()
    			             .mimeType(blob.getContentType())
    			             .acl("public-read")
    			             .addUserMetadata("myfield1", "my field value")
    			             .build();
    			             if(b.length< 1000000){
    			            	 String strstr=new String(b);
    			            	 res.getWriter().println(strstr);
    			            	 
    			             cache.put(blob.getFilename().toString(),strstr);
    			             }

    			             gcsService.createOrReplace(filename, options,ByteBuffer.wrap(b));
    			             in.close();
    			             }
    			             else{
    			            	 int length =(int) (blob.getSize()/2);
    			            	 //split array into 2
    			            	 byte[] b1 = new byte[length];
    			            	 BlobstoreInputStream in1 = new BlobstoreInputStream(blob.getBlobKey());
    			            		 in1.read(b1, 0, length);
    			            		 
    	    			             GcsFilename filename = new GcsFilename("gaebucket",blob.getFilename()+"_1");
    	    			             GcsFileOptions options = new GcsFileOptions.Builder()
    	    			             .mimeType(blob.getContentType())
    	    			             .acl("public-read")
    	    			             .addUserMetadata("myfield1", "my field value")
    	    			             .build();
    	    			             gcsService.createOrReplace(filename, options,ByteBuffer.wrap(b1));
    	    			             in1.close();
    	    			             BlobstoreInputStream in2 = new BlobstoreInputStream(blob.getBlobKey(),length);
    	    			             in2.read(b1, 0, length);		            		 
    	     			             GcsFilename filename1 = new GcsFilename("gaebucket",blob.getFilename()+"_2");
    	    			             GcsFileOptions options1 = new GcsFileOptions.Builder()
    	    			             .mimeType(blob.getContentType())
    	    			             .acl("public-read")
    	    			             .addUserMetadata("myfield1", "my field value")
    	    			             .build();
    	    			             in2.read(b1, 0, length);
    	    			             gcsService.createOrReplace(filename1, options1,ByteBuffer.wrap(b1));
    	    			             in2.close();
    			             }
    			         }
    			     }
    			 endTime=System.currentTimeMillis();
    			 res.getWriter().println("Total time taken with memcache:"+((endTime-startTime)/1000)+" sec");
   			 
			}
    	 
    }
	
