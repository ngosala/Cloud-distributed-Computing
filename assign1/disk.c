#include <pthread.h>

#include "ASSIGN1_H.h"

#define TESTRF "test_readfile.bin"
#define TESTWF "test_writefile.bin"

char *mgmntInfo;

RC create_Open_File(char *fileName,char *readfile) {

    FILE *fptr = fopen(fileName, "wb");
	FILE *fptr1 = fopen(readfile, "r");

	mgmntInfo=(char *)malloc(25*1024*1024);
    if (fptr == NULL) {

         return RC_FILE_NOT_FOUND;

    } else {

        printf("\nCreated Test File...\n");
		
		fseek(fptr1, 0L, SEEK_SET); 
		
		fread(mgmntInfo,1024, 25*1024*1024, fptr1);
		

        return RC_OK;

    }

}





void *testWriteDiskRandom_OneByte(void *args){

long l=0,i;

    static long array[25*1024*1024];



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "wb");  

    if (fptr == NULL) {

         exit(0);

    } else {

         

        fseek(fptr, 0L, SEEK_SET);        



        for(l=0;l<25*1024*1024;l++){

            array[l]=1;

        }

        fptr = fopen(fileName, "wb");        

         

        for(l=0;l<25*1024*1024;l++){

        i=random_gen(1,25*1024*1024);

        fseek(fptr,l,SEEK_SET);

        fwrite(array+l,1, 1, fptr);

        }

         

         

        

        

        fclose(fptr);

    }



}



void *testWriteDiskRandom_OneKByte(void *args){

long l=0,i;

    static long array[25*1024*1024];



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "wb");  

    if (fptr == NULL) {

         exit(0);

    } else {

         

        fseek(fptr, 0L, SEEK_SET);        



        for(l=0;l<25*1024*1024;l++){

            array[l]=1;

        }

        fptr = fopen(fileName, "wb"); 

        fseek(fptr, 0L, SEEK_SET);        

         

        for(l=0;l<25*1024;l++){

        i=random_gen(1,25*1024);

        fseek(fptr,l,SEEK_SET);

        fwrite(array+l,1024, 1, fptr);

        }

         



        

        

        fclose(fptr);

    }









}





void *testWriteDiskRandom_OneMByte(void *args){

long l=0,i;

    static long array[25*1024*1024];



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "wb");  

    if (fptr == NULL) {

         exit(0);

    } else {

         

        fseek(fptr, 0L, SEEK_SET);        



        for(l=0;l<25*1024*1024;l++){

            array[l]=1;

        }

        fptr = fopen(fileName, "wb"); 

        fseek(fptr, 0L, SEEK_SET);        

         

        for(l=0;l<100;l++){

        i=random_gen(1,100);

        fseek(fptr,l,SEEK_SET);

        fwrite(array+l,1024*1024, 1, fptr);

        }

         



        

        

        fclose(fptr);

    }









}

void *testWriteDiskSequential_OneByte(void *args){

     long l=0;

    static long array[25*1024*1024];



    FILE *fptr = NULL;

	char * fileName=(char *)args;

	
    fptr = fopen(fileName, "wb");

    if (fptr == NULL) {

         exit(0);

    } else {

    

        //printf("\nFile $%s  exist\n", fileName);

        fseek(fptr, 0L, SEEK_SET);        



        for(l=0;l<25*1024*1024;l++){

            array[l]=123;

        }

        



        fwrite(mgmntInfo,1, 25*1024*1024, fptr);



        fclose(fptr);

        

        

        

    }

}

void *testWriteDiskSequential_OneKByte(void *args){

     long l=0;

	 char * fileName=(char *)args;

    static long array[25*1024*1024];



    FILE *fptr = NULL;



    fptr = fopen(fileName, "wb");

    if (fptr == NULL) {

         exit(0);

    } else {


        fseek(fptr, 0L, SEEK_SET);        



        for(l=0;l<25*1024*1024;l++){

            array[l]=123;

        }

        



        fwrite(mgmntInfo,1024, 25*1024, fptr);



        fclose(fptr);

        

        

       

    }

}

void *testWriteDiskSequential_OneMByte(void *args){

     long l=0;

    static long array[25*1024*1024];



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "wb");

    if (fptr == NULL) {

         exit(0);

    } else {

    

        //printf("\nFile $%s  exist\n", fileName);

        fseek(fptr, 0L, SEEK_SET);        



        for(l=0;l<25*1024*1024;l++){

            array[l]=123;

        }

        



        fwrite(mgmntInfo,1024*1024, 25, fptr);



        fclose(fptr);

        

        

        



    }

}





void *testReadDiskSequential_OneByte(void *args){

    

     long l=0;

    void *tempdata1=malloc(25*1024*1024);



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "r");

    if (fptr == NULL) {

         exit(0);

    } else {

         

        



        fseek(fptr, 0L, SEEK_SET);



        fread(tempdata1,1, 25*1024*1024, fptr);



        fclose(fptr);

        

        

        

    }

        free(tempdata1);





}



void *testReadDiskSequential_OneKByte(void *args){

    char * fileName=(char *)args;

     long l=0;

    void *tempdata2=malloc(25*1024*1024);



    FILE *fptr = NULL;



    fptr = fopen(fileName, "r");

    if (fptr == NULL) {

         exit(0);

    } else {

        fseek(fptr, 0L, SEEK_SET);

        

        fread(tempdata2,1024, 25*1024, fptr);

         

        fclose(fptr);

        

        

        

    }



        free(tempdata2);





}

void *testReadDiskSequential_OneMByte(void *args){

    char * fileName=(char *)args;

     long l=0;

    void *tempdata3=malloc(25*1024*1024);

    FILE *fptr = NULL;



    fptr = fopen(fileName, "r");

    if (fptr == NULL) {

         exit(0);

    } else {

         

        



        fseek(fptr, 0L, SEEK_SET);

        

        fread(tempdata3,1024*1024, 25, fptr);

         

        fclose(fptr);
    }



        free(tempdata3); 

}


void *testReadDiskRandom_OneByte(void *args){

long l=0,i;

    void *tempdata1=malloc(25*1024*1024);



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "r");  

    if (fptr == NULL) {

         exit(0);

    } else {

         

        fseek(fptr, 0L, SEEK_SET);



        fptr = fopen(fileName, "r");        

        for(l=0;l<25*1024*1024;l++){

        i=random_gen(1,25*1024*1024);

        fseek(fptr,l,SEEK_SET);

        fread(tempdata1+l,1, 1, fptr);

        }

         

        

       

        fclose(fptr);

    }

    free(tempdata1);


}





void *testReadDiskRandom_OneKByte(void *args){

long l=0,i;



    void *tempdata2=malloc(25*1024*1024);



    FILE *fptr = NULL;

	char * fileName=(char *)args;

    fptr = fopen(fileName, "r");  

    if (fptr == NULL) {

         exit(0);

    } else {

         



        fptr = fopen(fileName, "r");

        fseek(fptr, 0L, SEEK_SET);      

         



        for(l=0;l<25*1024;l++){

        i=random_gen(1,25*1024);

        fseek(fptr,l,SEEK_SET);

        fread(tempdata2+l,1024, 1, fptr);

        }


        fclose(fptr);

    }



    free(tempdata2);



}



void *testReadDiskRandom_OneMByte(void *args){

long l=0,i;



    void *tempdata3=malloc(25*1024*1024);

	char * fileName=(char *)args;	

    FILE *fptr = NULL;



    fptr = fopen(fileName, "r");  

    if (fptr == NULL) {

         exit(0);

    } else {

         

        

        fptr = fopen(fileName, "r");

        fseek(fptr, 0L, SEEK_SET);        

         

        for(l=0;l<50;l++){

        i=random_gen(1,50);

        fseek(fptr,l,SEEK_SET);

        fread(tempdata3+l,1024*1024, 1, fptr);

        }  

         

        fclose(fptr);

    }



    free(tempdata3);



}



int main()

{

pthread_t pth1,pth2;

int i=0;



for(i=1;i<9;i++)

{

switch(i)

      {

printf("Enter");



case 1:

        printf("\nSTART: Disk Benchmarking: Read Sequential varying Block sizes and 1 thread\n");

        

		//Read Sequential one byte

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskSequential_OneByte,TESTRF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

        printf("\nSequential Read speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Read Sequential one kb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskSequential_OneKByte,TESTRF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

        printf("\nSequential Read speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Read Sequential one mb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskSequential_OneMByte,TESTRF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

        printf("\nSequential Read speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: Disk Benchmarking: Read Sequential varying Block sizes and 1 thread\n");                

		break;

case 2:

        printf("\nSTART: Disk Benchmarking: Read Sequential varying Block sizes and 2 thread\n");

        

		//Read Sequential one byte

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskSequential_OneByte,TESTRF);

        pthread_create(&pth2,NULL,testReadDiskSequential_OneByte,TESTRF);

        pthread_join(pth1,NULL);

		pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

        printf("\nSequential Read speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Read Sequential one kb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskSequential_OneKByte,TESTRF);

        pthread_create(&pth2,NULL,testReadDiskSequential_OneKByte,TESTRF);

        pthread_join(pth1,NULL);

		pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

        printf("\nSequential Read speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Read Sequential one mb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskSequential_OneMByte,TESTRF);

		pthread_create(&pth2,NULL,testReadDiskSequential_OneMByte,TESTRF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

        printf("\nSequential Read speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: DISK Benchmarking: Read Sequential varying Block sizes and 2 thread\n"); 

			

        break;	 


case 3:

		create_Open_File(TESTWF,TESTRF);

        printf("\nSTART: Disk Benchmarking: Write Sequential varying Block sizes and 1 thread\n");


        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskSequential_OneByte,TESTWF);

        pthread_join(pth1,NULL);

        gettimeofday(&end, 0x0);

		printf("\nSequential Write speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

        //Write sequential 1KB

		gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskSequential_OneKByte,TESTWF);

        pthread_join(pth1,NULL);

        gettimeofday(&end, 0x0);

        printf("\nSequential Write speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

       

		//Write sequential 1MB

		gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskSequential_OneMByte,TESTWF);

        pthread_join(pth1,NULL);

        gettimeofday(&end, 0x0);

        printf("\nSequential Write speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: Disk Benchmarking: Write Sequential varying Block sizes and 1 thread\n");

     

		break;

      

case 4: 

        printf("\nSTART: Disk Benchmarking: Write Sequential varying Block sizes and 2 threads\n");

      

        //Read sequential 1 Byte

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskSequential_OneByte,TESTWF);

        pthread_create(&pth2,NULL,testWriteDiskSequential_OneByte,TESTWF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

        gettimeofday(&end, 0x0);

        printf("\nSequential Write speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        

        //Read sequential 1KB

		gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskSequential_OneKByte,TESTWF);

        pthread_create(&pth2,NULL,testWriteDiskSequential_OneKByte,TESTWF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

        gettimeofday(&end, 0x0);

        printf("\nSequential Write speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

       

		//Read sequential 1MB

		gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskSequential_OneMByte,TESTWF);

        pthread_create(&pth2,NULL,testWriteDiskSequential_OneMByte,TESTWF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

        gettimeofday(&end, 0x0);

        printf("\nSequential Write speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: Disk Benchmarking: Write Sequential varying Block sizes and 2 thread\n");



		break;

case 5: 

	printf("\nSTART: DISK Benchmarking: Write Random varying Block sizes and 1 thread\n");


        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskRandom_OneByte,TESTWF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Write speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Write Sequential one kb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskRandom_OneKByte,TESTWF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Write speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Write Sequential one mb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskRandom_OneMByte,TESTWF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Write speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        printf("\nSTOP: DISK Benchmarking: Write Random varying Block sizes and 1 thread\n");                

		break;

case 6:

		printf("\nSTART: DISK Benchmarking: Write Random varying Block sizes and 2 thread\n");

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskRandom_OneByte,TESTWF);

        pthread_create(&pth2,NULL,testWriteDiskRandom_OneByte,TESTWF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Write speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskRandom_OneKByte,TESTWF);

        pthread_create(&pth2,NULL,testWriteDiskRandom_OneKByte,TESTWF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Write speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Write Sequential one mb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testWriteDiskRandom_OneMByte,TESTWF);

        pthread_create(&pth2,NULL,testWriteDiskRandom_OneMByte,TESTWF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Write speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: DISK Benchmarking: Write Random varying Block sizes and 2 thread\n");                

		break;

		

case 7:
		remove_file(TESTWF);
		printf("\nSTART: DISK Benchmarking: Read Random varying Block sizes and 1 thread\n");

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskRandom_OneByte,TESTRF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

        printf("\nRandom Read speed for Block Size 1B is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        

		//Write Sequential one kb

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskRandom_OneKByte,TESTRF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

        printf("\nRandom Read speed for Block Size 1KB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskRandom_OneMByte,TESTRF);

        pthread_join(pth1,NULL);

		gettimeofday(&end, 0x0);

        printf("\nRandom Read speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: DISK Benchmarking: Read Random varying Block sizes and 1 thread\n");                

		break;

case 8:

		printf("\nSTART: DISK Benchmarking: Read Random varying Block sizes and 2 threads\n");

        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskRandom_OneByte,TESTRF);

        pthread_create(&pth2,NULL,testReadDiskRandom_OneByte,TESTRF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Read speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));


        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskRandom_OneKByte,TESTRF);

        pthread_create(&pth2,NULL,testReadDiskRandom_OneKByte,TESTRF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Read speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));

        
        gettimeofday(&start, 0x0);

        pthread_create(&pth1,NULL,testReadDiskRandom_OneMByte,TESTRF);

        pthread_create(&pth2,NULL,testReadDiskRandom_OneMByte,TESTRF);

        pthread_join(pth1,NULL);

        pthread_join(pth2,NULL);

		gettimeofday(&end, 0x0);

		printf("\nRandom Read speed for Block Size 1MB is %f MB/s \n", (double)25*1000000/((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));



        printf("\nSTOP: DISK Benchmarking: Read Random varying Block sizes and 2 threads\n");                

		break;



default:

break;

}

} 

return 0;

}





long random_gen (long min, long max)

{

    long l=rand() % (max - min + 1) + min;

    return l;

}



RC remove_file(char *fileName){



    remove(fileName);

    return RC_OK;

}




