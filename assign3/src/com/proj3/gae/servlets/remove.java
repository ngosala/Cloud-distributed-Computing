package com.proj3.gae.servlets;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class remove extends HttpServlet {

	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
	String strFileContent = "";
	public static long startTime = 0, endTime = 0;

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		String strFilename = req.getParameter("key");
		res.getWriter().println("\n");
		res.getWriter().println("Remove file from cloud storage ");
		res.getWriter().println("\n");
		startTime = System.currentTimeMillis();
		GcsFilename GCfilename = new GcsFilename("gaebucket", strFilename);
		boolean result = gcsService.delete(GCfilename);
		if (result) {
			res.getWriter().println("File deleted from storage");
		}
		endTime = System.currentTimeMillis();
		res.getWriter().println(
				"Total time taken without memcache:"
						+ ((endTime - startTime)/1000 ) + "sec");
	}
}
