package com.proj3.gae.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;


@SuppressWarnings("serial")
public class cachesizemb extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		 MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		 Double size = (double) (cache.getStatistics().getTotalItemBytes());
		 	res.setContentType("text/plain");
	        res.getWriter().println("size in Bytes is : " + size);
	        res.getWriter().println("size in MB is : " + size/(1024*1024));
	}
	}