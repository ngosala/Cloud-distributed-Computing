rm -rf results
echo "creating Results Directory"
mkdir results

echo "Starting Memory Benchmark "
rm -rf *.out *.o

cc -pthread memory.c -o memory.out

./memory.out >results/Memory_Benchmark.txt

echo " Results are stored in /results directory"

echo "Starting CPU Benchmark "

java -jar cpu.jar > results/CPU_Benchmark.txt

echo " Results are stored in /results directory"

echo "Starting Disk Benchmark "

cc -pthread disk.c -o disk.out

./disk.out >results/Disk_Benchmark.txt


echo "Starting GPU Benchmark "

nvcc cuda.cu -o cuda.out

./cuda.out >results/cuda_Benchmark.txt


echo "Completed Benchmarking "
rm -rf *.out *.o