#include<stdio.h>
#include <cuda.h>
#include <sys/time.h>
#include <stdlib.h>

#define N 1024
struct timeval start,end;

void random_ints(int *a)
{
   int i;
   for (i = 0; i < N; i++){
    a[i] = rand();
	}
}

__global__ void add_gpu(int *a,int *b,int *c ) {

unsigned short tid = blockIdx.x ;

	if(tid<N){
	c[tid]+=a[tid]*b[tid];
	}
	  
} 

int main( void ) {

long i;
int *a,*b,*c,*m;
int *dev_a,*dev_b,*dev_c,*dev_m;

int size = N * sizeof( int ); 
long size_m = 100 * 1024 *1024; 

a = (int*)malloc( size ); 
b = (int*)malloc( size );
c = (int*)malloc( size );
m=	(int*)malloc( size_m );
cudaMalloc( (void**)&dev_a,  size );
cudaMalloc( (void**)&dev_b,  size );
cudaMalloc( (void**)&dev_c,  size );
cudaMalloc( (void**)&dev_m,  size_m );
random_ints(a);
random_ints(b);

memset(m,1,size_m);

cudaMemcpy( dev_a, a,  size, cudaMemcpyHostToDevice );
cudaMemcpy( dev_b, b,  size, cudaMemcpyHostToDevice );
gettimeofday(&start, 0x0);
add_gpu<<< N, 1 >>>( dev_a, dev_b, dev_c );
gettimeofday(&end, 0x0);
printf("IOPS GPU %f",(double)N*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));

cudaMemcpy( c, dev_c, size, cudaMemcpyDeviceToHost );

printf("\nMemory Bandwidth(write) with buffer size of 1B\n");
gettimeofday(&start, 0x0);
for(i=0;i<100*1024*1024;i++){

cudaMemcpy( m+i, dev_m+i, 1, cudaMemcpyHostToDevice );

}
gettimeofday(&end, 0x0);
printf("Memory Bandwidth(write) is %f MB/s \n",(double)100*1024*1024*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));

printf("\nMemory Bandwidth(write) with buffer size of 1KB\n");
gettimeofday(&start, 0x0);
for(i=0;i<100*1024;i++){

cudaMemcpy( dev_m+i, m+i, 1024, cudaMemcpyHostToDevice );

}
gettimeofday(&end, 0x0);
printf("Memory Bandwidth(write) is %f MB/s \n",(double)100*1024*1024*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));

printf("\nMemory Bandwidth(write) with buffer size of 1MB\n");
gettimeofday(&start, 0x0);
for(i=0;i<100;i++){

cudaMemcpy( dev_m+i, m+i, 1024*1024, cudaMemcpyHostToDevice );

}
gettimeofday(&end, 0x0);
printf("Memory Bandwidth(write) is %f MB/s \n",(double)100*1024*1024*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));


printf("\nMemory(read) Bandwidth with buffer size of 1B\n");
gettimeofday(&start, 0x0);
for(i=0;i<100*1024*1024;i++){

cudaMemcpy( m+i, dev_m+i, 1, cudaMemcpyDeviceToHost );

}
gettimeofday(&end, 0x0);
printf("Memory Bandwidth(read) is %f MB/s \n",(double)100*1024*1024*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));

printf("\nMemory Bandwidth(read) with buffer size of 1KB\n");
gettimeofday(&start, 0x0);
for(i=0;i<100*1024;i++){

cudaMemcpy( m+i, dev_m+i, 1024, cudaMemcpyDeviceToHost );

}
gettimeofday(&end, 0x0);
printf("Memory Bandwidth(read) is %f MB/s \n",(double)100*1024*1024*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));

printf("\nMemory Bandwidth(read) with buffer size of 1MB\n");
gettimeofday(&start, 0x0);
for(i=0;i<100;i++){

cudaMemcpy( m+i, dev_m+i, 1024*1024, cudaMemcpyDeviceToHost );

}
gettimeofday(&end, 0x0);
printf("Memory Bandwidth(read) is %f MB/s \n",(double)100*1024*1024*1000000/(1000000000*((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec))));

cudaFree( dev_a );
cudaFree( dev_b );
cudaFree( dev_c );
free(a);
free(b);
free(c);

return 0;
}