package com.proj3.gae.servlets;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class remove_all_mc extends HttpServlet {

	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
	private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	String strFileContent = "";
		public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		String strFilename = req.getParameter("key");
				res.getWriter().println("Remove Contents from memecache ");
				cache.clearAll();
				res.getWriter().println("Remove Contents from memecache completed ");
		}
}
