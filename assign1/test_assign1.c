#include <stdlib.h>
#include "ASSIGN1_H.h"

char *testName;

#define TESTF "test_file.bin"

int 
main (void) 
{
  testName = "";

  //testcpu(1);
  
  createFile(TESTF);
  testWriteDiskSequential(TESTF);
  testReadDiskSequential(TESTF);
  remove_file(TESTF);
  createFile(TESTF);
  testWriteDiskRandom(TESTF);
  testReadDiskRandom(TESTF);
  remove_file(TESTF);
  //testMemReadSequential();
  //testMemWriteSequential();
  //testMemReadRandom();
  //testMemWriteRandom();
  return 0;
}
