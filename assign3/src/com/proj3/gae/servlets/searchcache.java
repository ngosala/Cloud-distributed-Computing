package com.proj3.gae.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

public class searchcache extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
	private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		String strFilename = req.getParameter("key");
		String strFileContent = "";

		res.getWriter().println("Search for file in the Memcache");

		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
		int flag = -1;

		if (cache.get(strFilename) != null) {
			if (cache.contains(strFilename)) {
				res.getWriter().println(
						"File " + strFilename + " File Found in Memcache");
				flag = 1;
			} 
		}
		if (flag == -1) {
			res.getWriter().println(
					"File " + strFilename
							+ " File not Found in Memcache");
		}
		res.getWriter().println("Search for file in the memcache Completed");

	}

}
