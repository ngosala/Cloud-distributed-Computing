import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;



public class SQSExec {
	public static String instanceId;
	public static void main(String[] args) throws Exception {
		int count=5,i=0;
		long time=0;
		HashMap<String, Integer> mapPool = new HashMap<String, Integer>();
		List<String> instanceIds = new ArrayList<String>();
		AWSSQS.init();
		String SQSURL="https://sqs.us-west-2.amazonaws.com/521499112865/TaskQueue";
		String RESURL="https://sqs.us-west-2.amazonaws.com/521499112865/ResultQueue";
		String SQSIDLE="https://sqs.us-west-2.amazonaws.com/521499112865/IdleTime";
		
		String WorkerIdleCommand=AWSSQS.readMessage(SQSIDLE);
		while(WorkerIdleCommand==null){
			Thread.sleep(2000);
			WorkerIdleCommand=AWSSQS.readMessage(SQSIDLE);
		}
		//Parse Idle Time
		DynamoDB.init();
		String[] IdleCommand=WorkerIdleCommand.split(" ");
		long idleTime=1000 *(Long.parseLong(IdleCommand[2]));
		if(idleTime==0){
			while(true){
				
				String command=AWSSQS.readDelMessage(SQSURL);
				if(command!=null){
					mapPool.clear();
					String[] commandE=command.split(":");
						mapPool.put(command, Integer.parseInt(commandE[1]));
						ThreadExecutor.init(mapPool.size());
						ThreadExecutor.ExecuteTasks(mapPool);
				}
				else{
					Thread.sleep(5*1000);
				}
			}	
		}
		
		else{
			
			while(idleTime>0){
				
				String command=AWSSQS.readDelMessage(SQSURL);
				if(command!=null){
					mapPool.clear();
					String[] commandE=command.split(":");
						mapPool.put(command, Integer.parseInt(commandE[1]));
						ThreadExecutor.init(mapPool.size());
						ThreadExecutor.ExecuteTasks(mapPool);

				}
				else{
					Thread.sleep(5*1000);
					idleTime=idleTime-5*1000;
				}
				
			}
		}
		AWSSQS.init();
		instanceId=getInstanceId();
		instanceIds.add(instanceId);
		AWSSQS.TerminateInstance(instanceIds);	
	}
	
	public static String getInstanceId() throws IOException{
		String Id = "";
		String inputLine="";
		URL EC2MetaData = new URL("http://169.254.169.254/latest/meta-data/instance-id");
		URLConnection EC2MD = EC2MetaData.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(EC2MD.getInputStream()));
				while ((inputLine = in.readLine()) != null)
				{	
					Id = inputLine;
					System.out.println(Id);
				}
				in.close();
				return Id;
	}


	

}
