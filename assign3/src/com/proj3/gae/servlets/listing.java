package com.proj3.gae.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

public class listing extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
        String strValue=req.getParameter("key");
		res.getWriter().println("\n");
		res.getWriter().println("List of File present in the Cloud Storage");
		res.getWriter().println("\n");
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
        List<String> filenames= new ArrayList<String>(); 
		while (resultFiles.hasNext()) {
			ListItem listFile = resultFiles.next();
			String nameFile = listFile.getName().toString();
			if(nameFile.toLowerCase().contains(strValue.toLowerCase()))
			{
					filenames.add(nameFile);
			}

		}
		for(int i=0;i<filenames.size();i++)
		{
			res.getWriter().println(filenames.get(i).toString());
		}
		res.getWriter().println("\n");
		res.getWriter().println("List of Files Display Completed");
		res.getWriter().println("\n");
	}

}
