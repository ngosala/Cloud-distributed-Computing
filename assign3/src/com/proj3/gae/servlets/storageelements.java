package com.proj3.gae.servlets;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

@SuppressWarnings("serial")
public class storageelements extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
    private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		res.getWriter().println("\n");
		res.getWriter().println("Number of Files present in Cloud Storage");
		res.getWriter().println("\n");
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
	
		
       long no=0;
		while (resultFiles.hasNext()) {
			ListItem listFile = resultFiles.next();
			no++;
		}
		res.getWriter().println("Total number of files present in Cloud Storage: "+ no);
		res.getWriter().println("\n");
		res.getWriter().println("Number of files present in Cloud Storage Completed");
		res.getWriter().println("\n");
	}
}
