package com.proj3.gae.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

@SuppressWarnings("serial")
public class listfiles extends HttpServlet {

	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
    public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
    	res.setContentType("text/plain");
		res.getWriter().println("\n");
		res.getWriter().println("List of File present in the Cloud Storage");
		res.getWriter().println("\n");
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
	
       long no=0;
		while (resultFiles.hasNext()) {
			ListItem listFile = resultFiles.next();
			String nameFile = listFile.getName();
			res.getWriter().println("File"+no+":"+nameFile);
			res.getWriter().println("\n");
			no++;
		}
		res.getWriter().println("\n");
		res.getWriter().println("List of Files Display Completed");
		res.getWriter().println("\n");
	}
}