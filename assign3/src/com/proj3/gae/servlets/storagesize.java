package com.proj3.gae.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.*;

public class storagesize extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
	private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		res.getWriter().println("\n");
		res.getWriter().println("Storage size of Cloud Storage");
		res.getWriter().println("\n");
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);

		Double size = 0.0;
		while (resultFiles.hasNext()) {
			ListItem listFile = resultFiles.next();
			String namefile = listFile.getName();

			GcsFilename GCfilename = new GcsFilename("gaebucket", namefile);
			int fileSize = (int) gcsService.getMetadata(GCfilename).getLength();
			res.getWriter().println(fileSize);

			size += fileSize;
		}
		res.getWriter().println(
				"Size of Cloud Storage: " + size / (1024 * 1024) + " MB");
		res.getWriter().println("\n");
		res.getWriter().println("List of Files Display Completed");
		res.getWriter().println("\n");
	}
}
